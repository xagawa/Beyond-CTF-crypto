# Beyond-CTF-crypto



* Cocalc: https://cocalc.com/
* 資料置き場: https://gitlab.com/xagawa/Beyond-CTF-crypto
* NIST PQC標準化: https://csrc.nist.gov/Projects/Post-Quantum-Cryptography/Post-Quantum-Cryptography-Standardization}


* SageMath: 四元数環のマニュアル: http://doc.sagemath.org/html/en/reference/quat_algebras/
* SageMath: 線形代数チートシート: https://wiki.sagemath.org/quickref?action=AttachFile&do=get&target=quickref-linalg.pdf


* 時間が余った人向け
	* Compact LWE ver0
	* https://eprint.iacr.org/2017/685
	* 破り方
	* https://eprint.iacr.org/2017/742
	* https://rd.springer.com/chapter/10.1007/978-3-319-76953-0_5
	* ナップサック暗号の殺し方解説: https://www.slideshare.net/trmr105/katagaitai-workshop-7-crypto


