# Hint
p = 4294967279 # 13, 251, 65521, or 4294967279.
d = 64 # 16, 32, or 64
BOUND = 2**30 # to bound m,n,r,s
Q.<ii,jj,kk> = QuaternionAlgebra(GF(p),-1,-1)
F.<z> = PolynomialRing(GF(p))
# Q = QuaternionAlgebra(GF(p),a,b): i^2 = a, j^2 = b, ji = -ij, k = ij
print(p,d,Q)
A = Q.random_element()
f = F.random_element(degree=(0,d-1))
m = randint(0,BOUND)
f(A)

# Alice -> Bob

A = ...
B = ...
m = ...
n = ...
f = ...
X = ...

print A
print B
print X

# Bob -> Alice

r = ...
s = ...
h = ...
Y = ...

print Y

# Alice's Key

KA = ...
print "KA = ", KA

# Bob's Key

KB = ...
print "KB = ", KB
