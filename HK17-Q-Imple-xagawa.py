p = 4294967279 #13 , 251, 65521, or 4294967279.
d = 64 #16, 32, or 64
BOUND = 2**30 # to bound m,n,r,s
Q.<ii,jj,kk> = QuaternionAlgebra(GF(p),-1,-1)
# Q = QuaternionAlgebra(GF(p),a,b): i^2 = a, j^2 = b, ji = -ij, k = ij
F.<x> = PolynomialRing(GF(p))

def sample_poly(qa):
    f = F(0)
    while f(qa) == 0:
        f = F.random_element(degree=(0,d-1))
    return f

# Alice side:
A = Q.random_element()
B = Q.random_element()
m = randint(0,BOUND)
n = randint(0,BOUND)
f = sample_poly(A)
# f = F.random_element(degree=(0,d-1))
X = f(A)^m * B * f(A)^n

print "="*20
print "Alice -> Bob:"
print "A = ", A
print "B = ", B
print "X = ", X

# Bob side:
r = randint(0,BOUND)
s = randint(0,BOUND)
h = sample_poly(A)
Y = h(A)^r * B * h(A)^s

print "="*20
print "Bob -> Alice"
print "Y= ", Y

# Alice:
KA = f(A)^m * Y * f(A)^n
# Bob
KB = h(A)^r * X * h(A)^s

# Check!
print "="*20
print "f(A)^m * h(A)^r = ", f(A)^m * h(A)^r
print "h(A)^r * f(A)^m = ", h(A)^r * f(A)^m
print "f(A)^n * h(A)^s = ", f(A)^n * h(A)^s
print "h(A)^s * f(A)^n = ", h(A)^s * f(A)^n
print "="*20
print "KA = ", KA
print "KB = ", KB
