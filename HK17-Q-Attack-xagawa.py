p = 4294967279 #13 , 251, 65521, or 4294967279.
d = 64 #16, 32, or 64
BOUND = 2**30 # to bound m,n,r,s
Q.<ii,jj,kk> = QuaternionAlgebra(GF(p),-1,-1)
# Q = QuaternionAlgebra(GF(p),a,b): i^2 = a, j^2 = b, ji = -ij, k = ij
F.<x> = PolynomialRing(GF(p))

def sample_poly(qa):
    f = F(0)
    while f(qa) == 0:
        f = F.random_element(degree=(0,d-1))
    return f

# Alice side:
A = Q.random_element()
B = Q.random_element()
m = randint(0,BOUND)
n = randint(0,BOUND)
f = sample_poly(A)
X = f(A)^m * B * f(A)^n

print "="*20
print "Alice -> Bob:"
print "A = ", A
print "B = ", B
print "X = ", X

# Bob side:
r = randint(0,BOUND)
s = randint(0,BOUND)
h = sample_poly(A)
Y = h(A)^r * B * h(A)^s

print "="*20
print "Bob -> Alice"
print "Y= ", Y

# Alice:
KA = f(A)^m * Y * f(A)^n
# Bob
KB = h(A)^r * X * h(A)^s


# Check!
print "="*20
print "f(A)^m * h(A)^r = ", f(A)^m * h(A)^r
print "h(A)^r * f(A)^m = ", h(A)^r * f(A)^m
print "f(A)^n * h(A)^s = ", f(A)^n * h(A)^s
print "h(A)^s * f(A)^n = ", h(A)^s * f(A)^n
print "="*20
print "KA = ", KA
print "KB = ", KB



def make_vector(Z):
    return vector(GF(p),Z.coefficient_tuple())

def make_matrix(A,B):
    return matrix(GF(p),[make_vector(z) for z in [A*B*A,A*B,B*A,B]])

def solver(A,B,X):
    v = vector(GF(p),X.coefficient_tuple())
    # obtain s s.t. s*M=v
    s = make_matrix(A,B).solve_left(v)
    # we assume a = 1
    return [1, s[0]^(-1)*s[2], s[0], s[1]]


z = solver(A,B,X)
R = (z[0]*A+z[1]) * B * (z[2]*A+z[3])
K = (z[0]*A+z[1]) * Y * (z[2]*A+z[3])

print "="*20
print " X=", X
print " R=", R
print "="*20
print "KA=", KA
print "KB=", KB
print " K=", K
